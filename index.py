# homework 1
import random


def generate_password(num=6):
    password = ''
    for i in range(0, num):
        letter = random.randint(67, 120)
        password += chr(letter)
        yield password


gen = generate_password(8)
for i in gen:
    print(i)


def even_num_generator():
    password = ''
    while True:
        password += str(random.randint(0, 9))
        check = yield password
        if check == 'stop':
            print(password)

gen1 = even_num_generator()
next(gen1)
next(gen1)
next(gen1)
next(gen1)
next(gen1)
next(gen1)
next(gen1)
next(gen1)
next(gen1)
gen1.send('stop')
